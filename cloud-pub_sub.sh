## Console

# View the message
gcloud pubsub subscriptions pull --auto-ack MySub

## Python

sudo apt-get update
sudo apt-get install virtualenv
virtualenv -p python3 venv
source venv/bin/activate

# Install client library to GCP
pip install --upgrade google-cloud-pubsub
git clone https://github.com/GoogleCloudPlatform/python-docs-samples.git
cd python-docs-samples/pubsub/cloud-client/

# Pub/Sub - the Basics
# Create a topic
export GLOBAL_CLOUD_PROJECT=qwiklabs-gcp-04-dce36c5c4ce1
cat publisher.py
python publisher.py -h

python publisher.py $GLOBAL_CLOUD_PROJECT create MyTopic
python publisher.py $GLOBAL_CLOUD_PROJECT list

# Create a subscription
python subscriber.py $GLOBAL_CLOUD_PROJECT create MyTopic MySub
python subscriber.py $GLOBAL_CLOUD_PROJECT list_in_project
python subscriber.py -h

# Publish messages
gcloud pubsub topics publish MyTopic --message "Hello from cloud shell"
gcloud pubsub topics publish MyTopic --message "Publisher's name is Alberto Eyo"
gcloud pubsub topics publish MyTopic --message "Publisher likes to eat Tortilla"
gcloud pubsub topics publish MyTopic --message "Publisher thinks Pub/Sub is awesome"

# View messages
python subscriber.py $GLOBAL_CLOUD_PROJECT receive MySub
