//jshint strict: false
//jshint esversion: 6
'use strict'
const crc32 = require('fast-crc32c')
const gcs = require('@google-cloud/storage')()
const PubSub = require('@google-cloud/pubsub')
const imagemagick = require('imagemagick-stream')

exports.thumbnail = (event, context) => {
  const fileName = event.name
  const bucketName = event.bucket
  const size = '64x64'
  const bucket = gcs.bucket(bucketName)
  const topicName = 'messages-topic'
  const pubsub = new PubSub()
  if (fileName.search('64x64_thumbnail') === -1) {
    // doesn't have a thumbnail, get the filename extension
    var filenameSplit = fileName.split('.')
    var filenameExt = filenameSplit[filenameSplit.length - 1]
    var filenameWithoutExt = fileName.substring(0, fileName.length - filenameExt.length)
    if (filenameExt.toLowerCase() === 'png' || filenameExt.toLowerCase() === 'jpg') {
      // only support png and jpg at this point
      console.log(`Processing Original: gs://${bucketName}/${fileName}`)
      const gcsObject = bucket.file(fileName)
      let newFilename = filenameWithoutExt + size + '_thumbnail.' + filenameExt
      let gcsNewObject = bucket.file(newFilename)
      let srcStream = gcsObject.createReadStream()
      let dstStream = gcsNewObject.createWriteStream()
      let resize = imagemagick().resize(size).quality(90)
      srcStream.pipe(resize).pipe(dstStream)
      return new Promise((resolve, reject) => {
        dstStream
          .on('error', (err) => {
            console.log(`Error: ${err}`)
            reject(err)
          })
          .on('finish', () => {
            console.log(`Success: ${fileName} → ${newFilename}`)

            // set the content-type
            gcsNewObject.setMetadata(
              {
                contentType: 'image/' + filenameExt.toLowerCase()
              }, function(err, apiResponse) {})
            pubsub
              .topic(topicName)
              .publisher()
              .publish(Buffer.from(newFilename))
              .then(messageId => {
                console.log(`Message ${messageId} published.`)
              })
              .catch(err => {
                console.error('ERROR:', err)
              })

          })
      })
    } else {
      console.log(`gs://${bucketName}/${fileName} is not an image I can handle`)
    }
  } else {
    console.log(`gs://${bucketName}/${fileName} already has a thumbnail`)
  }
}